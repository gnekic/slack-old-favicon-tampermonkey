// ==UserScript==
// @name         Old Slack Favicon - Say no to swastika
// @namespace    https://slack.com/
// @version      0.3
// @description  try to take over the world!
// @author       gordan@neki.ch
// @match        https://*.slack.com/*
// ==/UserScript==

(function() {
    'use strict';

  	// Config settings
  	var CONFIG_PLAY_NOTIFICATION_SOUND = true;

  	// Inject sound
  	if (CONFIG_PLAY_NOTIFICATION_SOUND) {
      var playerNotification = document.createElement('audio');
      playerNotification.src = 'https://slack.global.ssl.fastly.net/08f7/sounds/call/confirmation_v2.mp3';
      //playerNotification.src = 'https://audiosoundclips.com/wp-content/uploads/2011/12/Lionroar.mp3';
      playerNotification.preload = 'auto';
    }

  	// Notification sound :D because hell yea why not lulz, inject lion ROAR for ultimate lulz
    function playSound(){
			if (CONFIG_PLAY_NOTIFICATION_SOUND) {
      	playerNotification.play();
    	}
    }

  	// Define new favicon for both notification and normal one
    function setFaviconToNormal(){
        var favIcon = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4wEWDSAsu5I5bQAAB3FJREFUWMPll9tvXFcVxn9rnzNnjj2+ZMZjO8RJnJRbW4deglsXKX2oQGouoopU8YYEaishJBC0XCJe+tAHBAiBBEECJB76WiSKUAQU9ZJbFZqkpUnjpiUXN9jY7rj2TJx4bufsvXg4Z26hUv+A7tFo9uzZZ69vfevb394DH/cmrc4vT10Ap2LDYAihzwgYETwEI+CJYKS733lYAYehWTpCtPwnrBqcc1inWKe49NNaV6usNdY9X/Tn3z8NgA+w5YlH+fwDU3zn8DMH908fODQ+PJo3oN0BW0FNq0/SB3CqOAwyuBXKWTxnsU4wTjE2AYJTEU/K46PZn84+tvf5n61V+OGP/50ACIoFvvq9rxeyW8YPLVaWZjZvGk/yEhARRAQQhM536QKgKZWmbxIT5HH1lS6OhaxRxnzLZj9iXJqH9vzm2WPepLfWZgABlH6ndmRhdYHPbZsi8Pz2GqqAKIqgCoqiIji0DUBVISii4W1ovUTWwJAXM+5HbPaabDIxAY4ociMXYvoHA+0CoIqqE0FYWV/hevU6Y4PFZDwFqO16Kw5poUrqn87L+lmKxbvIR8cZNU2GTExGLaqKOpJ3AlY0XdDvpJAQUW3WWCwvMTZYbA0nGSM4wCg4tP1bn2fIZwM292UZC0NyI9M0N3K42k1UwXUoSpLQNq9dAFqjCs5Z/rO6wNTEHfiel2SsSY1UQYzQ73mMhhm29IeMhQEDvocnKRv+BNHAFFpdAjXt5Dq17G1dDCiqigCl6yUqtXVGBvJYBSOQ8z3Gw4CJXMh4GDCY8fGNtB7FtRaXAH/TDHHp5UQX6bh2g9BbAGibAQWn3KzdZLm8xM78KJvDDNv6Q8b7kqCeCGIEax03q3UEyGYDfM/Dpoub4XuQzAhaL4GmeukKrHT6HRE6hwj0hQFDgzmKuYi9n8gzHCRBW7bjVDn35lv8/R8vsbC4jABjoyPsmrqDPV94gOJoEdO3FTNwB666DJgu5pMyoumuaQHYPjFGVK0x9qltDA0NkA0yRH6TpqshDOFS9XjG8Oa58zz9zE9YWV0jm83ieR5n36hy5K8v8IMnv81XHj1IbEK8/Axx6VgasIuBrrK0AYyODNMMA/LDA3h+QspaY4Mr6yuMh0M4FEFQlJePHmd+YZ7J7ZN88xuPM1LIc+nyVS5fucJ907tx6gDB5HdDJg/11bbAuzVBNwNOXfst6hAEp46LlSXuK+5MHC81IuccURTRbNbxPeHTn7yN6d334DSxJesSANK3DTNwO7Z2HPA6wdssdIsw3QEJAMWk9nj1xgprjQ1Gw8FkDvClLz7EsRMnmZt7j0M/eprJyW3svvce9u97mF27phBJM/X68Qoz2NLJnuBJrI4GPIDPHnwI24yG+wpDXxMjhZaH123EZK7Atlyhbbfj42PcN72bgVyOaq3K3Nx7nDnzOidOvMqOye3s3LkzXVxAfOLlF8HW0uDgHGulcvxsNiPrf/hbOUk2sVJNTrWUDVVH5GJmK4tE1qZZgLWW7Vu38uR3v8Xvf3uYX//qF9x11y7mFxZ4/s9/odmM0kAW+ieRwc+gzt4iRu3VgKprB1V1JCpItt7l9fdZa2xQDAfACC+9+ArPPfdHZmbuZ9euKYwxZHwPa2PiOG6XMylDDlOYwZZOdexQUs8XukWoqQg1FaFJ5gms1m9w9cYKxXAQay1nz77O0WPHefmVo4RhiIhQr9fJb9rEvr0P4/s+zqXnpCqmeD+SHUY3Ktgy1OYd1Xcsfn83A3QYcGqQNgdCw1pmywvsHplERHj8ice48847ee30aa5du0Ycx0xMTHBg/z4efHBPsgtEUCNoI8J+0E/9jSK1sys03xMaZcdGzSJGexno7AKX2GfqfgpcWl+m3KxSCHIUCiM88siX2X9gH416A6eOMAzxgyBhsd7EzpeIzr5D8/h54n9dov5+hYbTdCtrerxr7zbsLUH6SoGUautcXS9RGN2ZzLNJltlcf7JYtUHj8hzR6Ys0T5wnOncZ9/4aGsUggvGy4BkUmx7t2hZBR4SkDDiDiHZAIDRiy2x5nnuLOxBjkqAbdeK5JaLX3qZ54jzx+Su4lQpqbXJ8ioCXHOOiAaIBTqpdrvwhJVDncCZlQEmBGFTg3fVlymurDC5UaJ66QHTyLeLZOdzqdXDu/4Ki2nVtETzXT2w20t2XuOqHWLFCqwTGJP6/Uae5UObti29y9tpJpq9UiMo3wLmUDUmCp0z2XHBad0ZVjOsHMUDqKbdqQFVxkl7AbjaI5stUZxepzS7RWCgTbdT5pwxyd6bYzlR7rjs9962eIKAYzWA0QKn2zPQBaqUy1tm6d26+0ni3RO3iEtFiBVeNaNkyIlyQBmViCur1hKUHgt4CJr0DIHguxMpG5QNr63VxHQDv/u4I18caqzvmRw57dX0KGBZj0v8DrbMU/otyydW4W0Jsl5Jvzb77Gdqqh6YLri/gDj9V+WB1OsimR17a+rw+rFp/IDOwxRMvd2tmknrCg6aP2yWL4yOappbbdeFd12jjhWZpsWAy8Zlo/aNW+Ji0/wFnFnC781JVrgAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOS0wMS0yMlQxMzozMjo0NCswMTowMCUHXvsAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTktMDEtMjJUMTM6MzI6NDQrMDE6MDBUWuZHAAAAV3pUWHRSYXcgcHJvZmlsZSB0eXBlIGlwdGMAAHic4/IMCHFWKCjKT8vMSeVSAAMjCy5jCxMjE0uTFAMTIESANMNkAyOzVCDL2NTIxMzEHMQHy4BIoEouAOoXEXTyQjWVAAAAAElFTkSuQmCC";
        var newLink = document.getElementById('favicon');
        newLink.rel = 'shortcut icon';
        newLink.href = 'data:image/png;base64,'+favIcon;
    }

  	function setFaviconToNotification(){
        var favIcon = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4wEXCjQwPM7DlwAABvNJREFUWMO1l0uMXEcVhr9Tdfv27cd0Z14ex6/x2DFSPBM5dhx740gEkCBOhEDKAvFQkFggxAJkIVlssmDFBpBIJMQqAsQyTrAQIiiYQBJYJHbs2HFi7JlxPOOZ8Tx7Xv26XXVY3DsvexyCQ26rpOpS1f3/+us/554SPsVHQVf6ArLZHPm0gTcBlLsS+MWbF8VFYQkhZwSMCBbBCFgRjKzvry1WwGP49v7d4x+X5MryYP3gb8+d+srxw0+e7Cl3txvQ9YAroGalT9IH8Kp4zD2ptUpg14lnOpYayyfHKuNHt97Xk+xLQEQQEUAQ1v7LOgIKfH3P1v/5mATErBvJe3WdozOjxC7eOFmTCZr2VTWVXfHp+L0+Zg3EiyBMLUwxX51P9qsr4GuuUhS/IruCU/janvvv2azrFUCAarPG2Nz4KqimoMmOE0JeFZe2rPlkgRTcTsl7x42ZUfq3P0hgbbJjlYSGghghby3dUYZt+YgtUfh/IqCKqiLA5PwkldoCncV2nIIRKASWnihkeyGiJwppywQERhJlvGLuQQkBCe5wm1eWaktMzI3T197N1ijDznxETy4BtSKIEZzzLFXrCJDNhvcYhOsUUO8RgVwUUmor0FWI+dL97ZTDBHQl7XhVLpy/yJ//8ldGxyYQYEt3JwMvnebpr36Ze05EO7Z1seWBnZRKRbJhhjho0vQ1hBI+DUNrDOcvvMuzP/kpUzOzZLNZrLW8fa7KH//0Ck9/EgW6Osq0l4vYIBmabSwzuDBFT1TCowiCopx57R+MjI7Qu6uX7333O3R2tHP12hDXBgcZ+v0L7Nq1g8DauwJOvLj/2qXh+uf40dBGAl49Xj2iHkHw6nm/Ms6jXX1JxhNFEbz3xHFMs1knsMK+vXs4fOhhvHoUcN7j4hbq6kRRcRV4+vRA4rF4XXLZ4AHVlICmhhKGFqeYbSzTHbWtZr8vfP5x/v76GwwPX+fkj5+lt3cnhw4+zPEnvsjAQD8iybuweRb//QLND36W4HlNE5uux2dVqweOHyvnOkrPiJGOBF+ou5jeQgc7Cx1pGlZ6erbw6OFDFAsFqrUqw8PXeeuts7z++pvs7t1FX19fQiANstbEq+Bqq1nVe2Yn51q/+d2rlYWNqRhN06umLD2xb/FeZYzYueSlCs45dm7fzg9/8H1+/avneO6XP+fAgYcYGR3lpZdP02zGKZCDfC/S9hnUu2TXqmttsyNQ9ah6EhckoXdt4RazjWW6oiIqcObMa5x68RRHjh6hf/+DiDHkoizGCC4lutKwBUzHUdzkv9a+JqIbqpDbTKipCU0yT2CmvsjQ4hSd2SLeOy5dvMR7ly9zbXCQUqmE857FxUX29vXx1JNPYK1d9QuqmK4jSLaMLldwc1Ab8VQ/cHdXwKtBVjUQGs5xeW6Ugx27EOCb3/oGR44c5sMPb7C0tEgctygUCwz099M/0I9zDowBa9BGjJvOUz/XRe3tKZrXhcacZ7nmPioKfFoqrGW/4cUp5ptVykFEW7HII48c4uDBA7iWQ4HAWkwQgAgujvHjs7TOXqH5t/O03rlK/VaFhtc0lD/WESQ/o0JgA6pxg1vLFdpKWxJDJakRYxIPq3O0lmv4iVncxWGab1zEvTuEG5uGRoyx2UQRXPpp100KEhIFvNe1pARkxZK3AdO1ebxPTYYmRJpN/FIVNzOPm5jBzy7gvUM62pDuMqarDbIW0RDRMFm3MQhu84D3eONXdy8WcjZDKZOj7mLiRoOg5dGWA7cutAQkHyU78h4aTbRaw9dqUK3BksP6PC2zvFre3fUISI/AWMEai204ctUGcWWGpdmAUi4HuSwEFrEmKSvVoMYjZJFSHukqY3raMRPT6NwCvlbHuALoDJDmlE1NKGkBttSgeXOe5vUFwnpIc+t22LeXRr6FrgBbk1QqK44yAtYgUYgUc1DKJy2XQTIG0wwxGqJUN/8a1heW6vbCSKVxZZLa++PIdJVyoY32vb3YrTvIhBkaVpIAIQ2SFXCRjVedwCAZi4QBhBY1SQFufYST5cq0c/U7CJz9w8szu0c6n7d1PYFQjqKcmLKxQRhkwijMZjJBqNbYlhEbCqJrlyxJwmc13asXaTlDHIs24kDi2KrzON/0YWUU//yJyvTMplez0IRBMVPcZsUWduzckTn2+LH2xz772N7de3YPtHe0749y0Y5MEJSNSA6RYF0UKaotVW34lltwzXi8sbh0bX5k7NLkpSuDN/55dnLy8tXa2NjN+Veak2ODrtb6r5fT6enpAGgH+oAjwEPAPqAHaEvVW1HQAU1gEZgChoCLwDvAdWAaqHV1dfmPLsvvuDcQA8vAOLAFuA8I0ytCLlVAgRZQA2aAm8BVYBCYSEnF3OXG/B/QKbDIuJBFWwAAAABJRU5ErkJggg==";
        var newLink = document.getElementById('favicon');
        newLink.rel = 'shortcut icon';
        newLink.href = 'data:image/png;base64,'+favIcon;
    }

  	var shouldIPlaySound = true;
    setInterval(function(){
      try{
        if (typeof unsafeWindow.TS === "undefined") { return; }
        var shouldIStayOrShouldIGo = unsafeWindow.TS.view.current_unread_status === 'mentions';

        if (shouldIStayOrShouldIGo) {
        	setFaviconToNotification();
          if (shouldIPlaySound) {
          	playSound();
            shouldIPlaySound = false;
          }
        }else{
          setFaviconToNormal();
          shouldIPlaySound = true;
        }

        //console.log('shouldIStayOrShouldIGo', shouldIStayOrShouldIGo);
        //console.log('shouldIPlaySound', shouldIPlaySound);
        //console.log('feature_incremental_unread_counts', unsafeWindow.TS.boot_data.feature_incremental_unread_counts);
        //console.log('current_unread_status', unsafeWindow.TS.view.current_unread_status);
      }catch(e){
      	console.log(e);
      }
    }, 150);

})();
